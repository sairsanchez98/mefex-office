<?php

namespace App\Http\Controllers\solicitarServicio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\solicitud;

class mainController extends Controller
{
    public function viewMenEmpresarial(){
        return view('guest/solicitarServicio/mensajeriaEmpresarial');
    }

    public function viewMenEcomm(){
        return view('guest/solicitarServicio/mensajeriaEcommerce');
    }

    public function viewMensajeria(){
        return view('guest/solicitarServicio/mensajeria');
    }

    public function RegistrarSolicitudMensajeria(Request $request){
        
        $data = json_decode( $request->data , true);
        
        
        $create = DB::table('table_solicitudes_mensajeria')->insert([
            "ciudad_remitente" => $data["ciudad_remitente"],
            "nombre_remitente"=> $data["nombre_remitente"],
            "cedula_remitente" => $data["cedula_remitente"],
            "barrio_remitente"=> $data["barrio_remitente"],
            "telefono_remitente"=>$data["telefono_remitente"],
            "direccion_remitente"=>$data["direccion_remitente"],
            
            "actividad_mensajero" => $data["actividad_mens_selected"],
            "descrip_activ_mensajero" => $data["descripcion_actividad"],

            "ciudad_receptor" => $data["ciudad_receptor"],
            "nombre_receptor"=> $data["nombre_receptor"],
            "cedula_receptor"=> $data["cedula_receptor"],
            "barrio_receptor"=> $data["barrio_receptor"],
            "telefono_receptor"=>$data["telefono_receptor"],
            "direccion_receptor"=>$data["direccion_receptor"],

            "return_mensajero" => $data["mensajero_return"],
            "created_at"=> date("Y-m-d H:i:s")
            
        ]);

        $solicitudes = solicitud::all();
        $ultima_solicitud = $solicitudes->last();
        //dd($ultima_solicitud);

        $createEstado = DB::table('table_estados_solicitudes')->insert([ 
            "estado" => "Recibido",
            "id_solicitud" => $ultima_solicitud->id,
            "created_at"=> date("Y-m-d H:i:s")
        ]);
        
            if ($create) {
                return "ok";# code...
            }else{
                return "err500";
            }

        
    }

    public function GetSolicitudes (){
        //$SOLICITUDES = DB::table('table_solicitudes_mensajeria')->get();

        $SOLICITUDES = DB::table('table_solicitudes_mensajeria')->get();


      

        $DatosJson = '{ "data": [ ';
            for ($i=0; $i < count($SOLICITUDES) ; $i++) { 
                    $id_solicitud = $SOLICITUDES[$i]->id;
                    $Estados = DB::table('table_estados_solicitudes')
                    ->where('table_estados_solicitudes.id_solicitud' , $id_solicitud )
                    ->get();
                    
                    foreach ($Estados as $key => $estado) {}

                    if($estado->estado == "Recibido"){
                        $ESTADO = "<button class='btn btn-info'>¡Nueva!</button>";
                    }else{
                        $ESTADO = "<button class='btn btn-default'>".$estado->estado."</button>";
                    }


                    $acciones = "<div class='button-list'>";
                    $acciones .= "<button class='EditSolicitud btn btn-icon btn-icon-circle btn-warning btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-edit'></i></span></button>";
                    $acciones .= "</div>";
                
                    $DatosJson .= '[
                        "000'.($SOLICITUDES[$i]->id).'",
                        "'.($SOLICITUDES[$i]->id).'",
                        "'.($SOLICITUDES[$i]->created_at).'",
                        "'.($SOLICITUDES[$i]->ciudad_remitente).'",
                        "'.($SOLICITUDES[$i]->nombre_remitente).'",
                        "'.($SOLICITUDES[$i]->cedula_remitente).'",
                        "'.($SOLICITUDES[$i]->barrio_remitente).'",
                        "'.($SOLICITUDES[$i]->telefono_remitente).'",
                        "'.($SOLICITUDES[$i]->direccion_remitente).'",
                        "'.($SOLICITUDES[$i]->actividad_mensajero).'",
                        "'.($SOLICITUDES[$i]->descrip_activ_mensajero).'",
                        "'.($SOLICITUDES[$i]->ciudad_receptor).'",
                        "'.($SOLICITUDES[$i]->nombre_receptor).'",
                        "'.($SOLICITUDES[$i]->cedula_receptor).'",
                        "'.($SOLICITUDES[$i]->barrio_receptor).'",
                        "'.($SOLICITUDES[$i]->telefono_receptor).'",
                        "'.($SOLICITUDES[$i]->direccion_receptor).'",
                        "'.($SOLICITUDES[$i]->return_mensajero).'",

                        "'.($acciones).'",

                        "'.($SOLICITUDES[$i]->direccion_remitente . ' Br/ '. $SOLICITUDES[$i]->barrio_remitente).'",
                        "'.($SOLICITUDES[$i]->direccion_receptor . ' Br/ '. $SOLICITUDES[$i]->barrio_receptor).'",
                        "'.($ESTADO).'"
                    ],';
                
            }
            $DatosJson = substr($DatosJson , 0 , -1); ## en los JSON no pueden terminar con una (,) y si revisamos bien en el foreach siempre va a terminar con uns (,) y por eso estoy substrayendo eso para quedar con el ultimo registro sin (,)        
            $DatosJson .= ' ] }';
        echo $DatosJson;
    }


    public function  GetEstadosSolicitud(Request $request){
        $id_solicitud = $request->id_solicitud;
//        $Estados = DB::table('table_estados_solicitudes')->where("id_solicitud", $id_solicitud)->get();
        $estadoRevisado = DB::table('table_estados_solicitudes')
                        ->where("id_solicitud", $id_solicitud)
                        ->where("estado", "Revisando")
                        ->get();
        if (count($estadoRevisado) == 0) {
            $update_state =  DB::table('table_solicitudes_mensajeria')
            ->where('id', $id_solicitud)
            ->update(['n_guia' => 1]);

            // $createEstado = DB::table('table_estados_solicitudes')->insert([ 
            //     "estado" => "Revisando",
            //     "id_solicitud" => $id_solicitud,
            //     "created_at"=> date("Y-m-d H:i:s")
            // ]);
        }
        $Estados = DB::table('table_estados_solicitudes')->where("id_solicitud", $id_solicitud)->get();
        
        return $Estados;
    }

    public function AplicarNuevoEstado(Request $request){
        $id_solicitud = $request->id_solicitud;
        $estado  =  $request->estado;
        $createEstado = DB::table('table_estados_solicitudes')->insert([ 
            "estado" => $estado,
            "id_solicitud" => $id_solicitud,
            "created_at"=> date("Y-m-d H:i:s")
        ]);
        if ($createEstado) {
            return "ok";
        }
    }

    public function ConsultarNuevasSolicitudes(Request $request){
        $SOLICITUDES = DB::table('table_solicitudes_mensajeria')
        ->where("n_guia" , null)->get();

        
        return count($SOLICITUDES);
        
    }

}
