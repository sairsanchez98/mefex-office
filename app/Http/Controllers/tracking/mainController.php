<?php

namespace App\Http\Controllers\tracking;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class mainController extends Controller
{
    public function index(){
        return view("guest.solicitarServicio.tracking");
    }

    public function seguirPedido (Request $request){
        $cedula = $request->cedula_remitente;
        
        $consulta1 = DB::table('table_solicitudes_mensajeria')
        ->where("table_solicitudes_mensajeria.cedula_remitente" , "=", $cedula)
        ->get();

        $consulta2 = DB::table('table_solicitudes_mensajeria')
        ->join("table_estados_solicitudes","table_solicitudes_mensajeria.id", "=", "table_estados_solicitudes.id_solicitud")
        ->select("table_estados_solicitudes.estado", "table_estados_solicitudes.created_at as created_estado" ,  "table_solicitudes_mensajeria.*" )
        ->where("table_solicitudes_mensajeria.cedula_remitente" , "=", $cedula)
        ->orderBy('table_solicitudes_mensajeria.id', 'desc')
        ->get();

        //dd($consulta2);

    
       


        if(count($consulta1) > 0 && count($consulta2)> 0){
            return ["Solicitud_encontrada_con_estados" , $consulta2];
        }

        else if(count($consulta1) > 0 && count($consulta2) == 0){
            return ["Solicitud_enviada_aun_no_vista" , $consulta1];
        }

        else if(count($consulta1) == 0 && count($consulta2) == 0){
            return "Solicitud_no_encontrada";
        }

        
    }
}
