<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class solicitud extends Model
{
    protected $table = 'table_solicitudes_mensajeria';
    protected $primaryKey = 'id';
    public $timestamps = true;
    
}
