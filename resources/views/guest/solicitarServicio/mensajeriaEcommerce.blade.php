@extends('layouts.app')
@section('title' , 'SOLICITAR SERVICIO')

@section('content')
<div class="hk-pg-wrapper" id="SolicitudServicios">
    <!-- Container -->
    <div class="container mt-xl-50 mt-sm-30 mt-15">
        <!-- Title -->
        <div class="hk-pg-header">
            <div>
                <h2 class="hk-pg-title font-weight-600 mb-10">Solicitar servicio</h2>
                <p>Recogemos, llevamos, entregamos, radicamos y pagamos lo que necesites en minutos.<i class="ion ion-md-help-circle-outline ml-5" data-toggle="tooltip" data-placement="top" title="Need help about earning stats"></i></p>
            </div>
            {{-- <div class="d-flex">
                <div class="btn-group btn-group-sm" role="group">
                    <button type="button" class="btn btn-outline-secondary active">today</button>
                    <button type="button" class="btn btn-outline-secondary">week</button>
                    <button type="button" class="btn btn-outline-secondary">month</button>
                    <button type="button" class="btn btn-outline-secondary">quarter</button>
                    <button type="button" class="btn btn-outline-secondary">year</button>
                </div>
            </div> --}}
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                
                
                <div class="card hk-dash-type-1 overflow-hide">
                    <div class="card-body ">
                        <div class="row d-flex justify-content-center mb-30">
                            <H4 class="text-center">Servicio de Mensajería Ecommerce</H4>
                        </div>
                        <div class="row d-flex justify-content-center">
                           
                        </div>

                    </div>
                </div>
             
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
  
</div>
@endsection  