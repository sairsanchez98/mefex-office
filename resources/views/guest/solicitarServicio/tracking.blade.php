@extends('layouts.app')
@section('title' , 'SOLICITAR SERVICIO')

@section('content')
<div  id="Seguimiento">
    <!-- Container -->
    <div class="container ">
        <!-- Title -->
        <div class="hk-pg-header">
            <div>
                <h2 class="hk-pg-title font-weight-600 mb-10">Seguimiento de servicio</h2>
                <p>Recogemos, llevamos, entregamos, radicamos y pagamos lo que necesites en minutos.<i class="ion ion-md-help-circle-outline ml-5" data-toggle="tooltip" data-placement="top" title="Need help about earning stats"></i></p>
            </div>
            {{-- <div class="d-flex">
                <div class="btn-group btn-group-sm" role="group">
                    <button type="button" class="btn btn-outline-secondary active">today</button>
                    <button type="button" class="btn btn-outline-secondary">week</button>
                    <button type="button" class="btn btn-outline-secondary">month</button>
                    <button type="button" class="btn btn-outline-secondary">quarter</button>
                    <button type="button" class="btn btn-outline-secondary">year</button>
                </div>
            </div> --}}
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                
                
                <div class="card hk-dash-type-1 overflow-hide">
                    <div class="card-body ">
                        <div class="row d-flex justify-content-center mb-30">
                            <H4 class="text-center">Digita el número de tu guía o número de cédula.</H4>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-6 form-group">
                                <input v-model="nguia" class="form-control" id="nombre" placeholder="Ej: 123456789" value="" type="text">
                            </div>
                            <div class="col-md-2 form-group">
                                <button v-if="!processing" @click="Search()" class="btn btn-primary"> Buscar <i class="fa fa-search"></i></button>
                                <button v-if="processing"  class="btn btn-primary"> Buscando... <i class="fa fa-search"></i></button>                                
                            </div>
                        </div>


                        <div v-show="view_solicitud_no_vista || view_solicitud_en_proceso">
                            <h5 class="hk-sec-title mb-25">Seguimiento de tus solicitudes:</h5>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="accordion accordion-type-2" id="accordion_2">
                                        
                                        <div  v-for="(ssr , index) in solicitudes_en_proceso " class="card">
                                            <div class="card-header d-flex justify-content-between">
                                                <a class="collapsed" role="button" data-toggle="collapse" :href="'#'+index" aria-expanded="false"><p v-text="ssr.created_solicitud"></p></a>
                                            </div>
                                            <div :id="index" class="collapse" data-parent="#accordion_2">
                                                <div class="card-body pa-15"> 
                                                    <h3 v-text="'Ruta: '  + ssr.barrio_remitente+ ' '+ ssr.direccion_remitente+ ' - '+ ssr.barrio_receptor+ ' '+ ssr.direccion_receptor "></h3><br>
                                                    <ul>
                                                        <li><b>Servicio:</b>  @{{ssr.actividad_mensajero}}</li>
                                                        <li><b>Actividad mensajero:</b>  @{{ssr.descrip_activ_mensajero}} </li>
                                                        <li><b>Receptor:</b>  @{{ssr.nombre_receptor}} </li>
                                                        {{-- <li>Tel. Receptor:  <p v-text= "ssr.telefono_receptor"></p> </li> --}}
                                                        
                                                    </ul>
                                                    <div class="row">
                                                        <div class="col">
                                                         <ul class="list-group mb-15">
                                                            <li v-for="estado in ssr.Estados" class="mb-5 mt-5 list-group-item list-group-inv" :class=" {'list-group-inv-primary' : estado.estado  == 'Finalizado'
                                                            ,'list-group-inv-danger' : estado.estado  == 'Cancelado' ,'list-group-inv-warning' : estado.estado  == 'Recibido' ,'list-group-inv-info' : estado.estado  == 'Revisando'  , 'list-group-inv-success' : estado.estado  == 'En curso' }"  >
                                                                <p v-text = " estado.fecha_hora +' | '+ estado.estado "></p>
                                                            </li>
                                                         </ul>
                                                        </div>
                                                    </div>    
                                                </div>
                                                
                                            </div>
                                        </div>


                                       
                                    </div>
                                  
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
             
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
  
</div>

<script>
    Sair = new Vue({
        el: "#Seguimiento",
        data:{
            nguia : null,
            view_solicitud_no_vista : false,
            view_solicitud_en_proceso : false,

            solicitudes_sin_revisar :false,
            solicitudes_en_proceso : false,
             processing : false,
             estados_solicitud_selected :[]
        },
        methods: {
            Search : function (){
                if(Sair.nguia){
                    Sair.processing = true;
                    let formdata =  new FormData();
                    formdata.append("cedula_remitente" , Sair.nguia);
                    axios.post("{{ route('guest.trackingIT') }}" ,formdata).then(function (response){
                        Sair.processing = false;
                        Sair.solicitudes_sin_revisar = false;
                        Sair.solicitudes_en_proceso = false;
                        if(response.data[0] == "Solicitud_enviada_aun_no_vista"){
                           Sair.solicitudes_sin_revisar = response.data[1];
                           Sair.view_solicitud_no_vista = true;
                           Sair.view_solicitud_en_proceso = false;
                        }else if(response.data[0] == "Solicitud_encontrada_con_estados"){
                            Sair.solicitudes_en_proceso = response.data[1];
                            Sair.view_solicitud_en_proceso = true;
                            Sair.view_solicitud_no_vista = false;

                            var nuevoArray    = []
                            var arrayTemporal = []
                            for(var i=0; i<Sair.solicitudes_en_proceso.length; i++){
                                arrayTemporal = nuevoArray.filter(resp => resp["id_solicitud"] == Sair.solicitudes_en_proceso[i]["id"])
                                if(arrayTemporal.length>0){
                                    nuevoArray[nuevoArray.indexOf(arrayTemporal[0])]["Estados"].push({"estado":Sair.solicitudes_en_proceso[i]["estado"] , "fecha_hora": Sair.solicitudes_en_proceso[i]["created_estado"] })
                                }else{
                                    nuevoArray.push({
                                        "id_solicitud" : Sair.solicitudes_en_proceso[i]["id"] , 
                                        "nombre_remitente" : Sair.solicitudes_en_proceso[i]["nombre_remitente"] , 
                                        "direccion_remitente" : Sair.solicitudes_en_proceso[i]["direccion_remitente"] , 
                                        "barrio_remitente" : Sair.solicitudes_en_proceso[i]["barrio_remitente"] , 
                                        "actividad_mensajero" : Sair.solicitudes_en_proceso[i]["actividad_mensajero"] , 
                                        "descrip_activ_mensajero" : Sair.solicitudes_en_proceso[i]["descrip_activ_mensajero"] , 
                                        "nombre_receptor" : Sair.solicitudes_en_proceso[i]["nombre_receptor"] , 
                                        "barrio_receptor" : Sair.solicitudes_en_proceso[i]["barrio_receptor"] , 
                                        "telefono_receptor" : Sair.solicitudes_en_proceso[i]["telefono_receptor"] , 
                                        "direccion_receptor" : Sair.solicitudes_en_proceso[i]["direccion_receptor"] , 
                                        "return_mensajero" : Sair.solicitudes_en_proceso[i]["return_mensajero"] , 
                                        "created_solicitud" : Sair.solicitudes_en_proceso[i]["created_at"] , 
                                        "Estados" : [{"estado":Sair.solicitudes_en_proceso[i]["estado"] , "fecha_hora": Sair.solicitudes_en_proceso[i]["created_estado"] }]})
                                }
                            }
                            Sair.solicitudes_en_proceso = nuevoArray;

                            //console.log(nuevoArray)


                        }else if(response.data[0] == "Solicitud_no_encontrada"){
                            Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Este número de cédula no está asociado a una solicitud'
                            })
                        }
                    })
                }else{
                    Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Por favor digita tu número de guía para continuar!'
                    })
                }
            },
            // getEstadosSolicitud (){
            //     Sair.estados_solicitud_selected = [];

            //     for (let index = 0; index < solicitudes_en_proceso.length; index++) {
            //         let formdata = new FormData();
            //         formdata.append("id_solicitud" , Sair.solicitudes_en_proceso[index]["id"]);
            //         axios.post("{{ route('web.GetEstadosSolicitud') }}" , formdata).then(function(response){
            //             Sair.estados_solicitud_selected += response.data;
            //         })
            //     }
                
            // },
        },
    })
</script>

@endsection  