@extends('layouts.app')
@section('title' , 'SOLICITAR SERVICIO')

@section('content')

<div  id="SolMensajeria">
    <!-- Container -->
    <div class="container ">
        <!-- Title -->
        <div class="hk-pg-header">
            <div>
                <h2 class="hk-pg-title font-weight-600 mb-10">Solicitar servicio</h2> <button @click="ver_tutorial()" class="btn btn-primary"><i class="fa fa-play"> </i> VER TUTORIAL</button>
                <p>Recogemos, llevamos, entregamos, radicamos y pagamos lo que necesites en minutos.
                    {{-- <i class="ion ion-md-help-circle-outline ml-5" data-toggle="tooltip" data-placement="top" title="Need help about earning stats"></i> --}}
                </p>
            </div>
            {{-- <div class="d-flex">
                <div class="btn-group btn-group-sm" role="group">
                    <button type="button" class="btn btn-outline-secondary active">today</button>
                    <button type="button" class="btn btn-outline-secondary">week</button>
                    <button type="button" class="btn btn-outline-secondary">month</button>
                    <button type="button" class="btn btn-outline-secondary">quarter</button>
                    <button type="button" class="btn btn-outline-secondary">year</button>
                </div>
            </div> --}}
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                
                
                <div class="card hk-dash-type-1 overflow-hide">
                    <div class="card-body ">
                        <div class="row d-flex justify-content-center mb-5">
                            <H4 class="text-center">Servicio de Mensajería Personal</H4>
                        </div>
                        <hr>
                        <div class="row d-flex justify-content-center mb-30">
                            
                            <h5 class="text-center" v-text="step_subtitulo"></h5>
                            
                        </div>
                        
                        <div class="row d-flex justify-content-center">
                            <div class="col">
                                <div v-show = "steps.datos_cliente">
                                    <div class="row">
                                        <div class="col-md-4 form-group">
                                            <label for="ciudad">Ciudad</label>
                                            <select v-model="datos.ciudad_remitente" name="" id="ciudad" class="form-control">
                                                <option value="monteria" selected>Montería</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="nombre">Nombre completo</label>
                                            <input v-model="datos.nombre_remitente" class="form-control" id="nombre" placeholder="" value="" type="text">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="cedula">Cédula</label>
                                            <input v-model="datos.cedula_remitente" class="form-control" id="cedula" placeholder="" value="" type="text">
                                        </div>
                                    </div>
                                    <div class="row d-flex justify-content-end mt-20" 
                                    v-if="datos.cedula_remitente && datos.ciudad_remitente && datos.nombre_remitente">
                                        <button @click="next_step(1)" class="btn btn-primary">Siguiente <i class="fa fa-angle-right"></i></button>
                                    </div>
                                </div>

                                <div v-show = "steps.lugar_origen">
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <label for="Barrio">Barrio</label>
                                                <select class="select2remitente select2 form-control" name="" id="">
                                                    <option value=""></option>
                                                    <option v-for="barrio in BarriosDB" :value="barrio" v-text="barrio"></option>
                                                </select>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <label for="Teléfono">Teléfono</label>
                                                <input v-model="datos.telefono_remitente" class="form-control" id="Teléfono" placeholder="" value="" type="text">
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <label for="Dirección">Dirección</label>
                                                <input v-model="datos.direccion_remitente" class="form-control" id="Dirección" placeholder="" value="" type="text">
                                            </div>
                                        </div>

                                        <div class="row ">
                                            <div class="col-6 d-flex justify-content-start ">
                                                <button @click="prev_step(1)" class="btn btn-primary"><i class="fa fa-angle-left"></i> Anterior</button>
                                            </div>
                                            <div class="col-6 d-flex justify-content-end"
                                            v-if="datos.barrio_remitente && datos.telefono_remitente && datos.direccion_remitente">
                                                <button @click="next_step(2)" class="btn btn-primary">Siguiente <i class="fa fa-angle-right"></i></button>
                                            </div>
                                        </div>
                                </div>

                                <div v-show = "steps.actividad_mensajero">
                                    <div class="row d-flex justify-content-center">
                                        <div v-for ="(am , index) in actividades_mensajero" class="col-md-2 form-group cursor_pointer">
                                            <div :class="['card bg-light', am.selected ] " @click="SelectActivMensajero(index)">
                                                <center>
                                                    <div class="card-header"><p v-text="am.name"></p></div>
                                                    <div class="card-body">
                                                        <img v-if="index == 0" width="50" class="img img-fluid" src="{{asset('dist/img/iconos/services/address.svg')}}" alt="">
                                                        <img v-if="index == 1" width="50" class="img img-fluid" src="{{asset('dist/img/iconos/services/paper.svg')}}" alt="">
                                                        <img v-if="index == 2" width="50" class="img img-fluid" src="{{asset('dist/img/iconos/services/delivery-man.svg')}}" alt="">
                                                        <img v-if="index == 3" width="50" class="img img-fluid" src="{{asset('dist/img/iconos/services/survey.svg')}}" alt="">
                                                        <img v-if="index == 4" width="50" class="img img-fluid" src="{{asset('dist/img/iconos/services/wallet.svg')}}" alt="">
                                                    </div>
                                                </center>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12">
                                            <label for="descripcion_actividad">Descríbe la la actividad del mensajero...</label>
                                            <input v-model="datos.descripcion_actividad" class="form-control" type="text" id="descripcion_actividad">
                                        </div>

                                    </div>

                                

                                    <div class="row mt-10">
                                        <div class="col-6 d-flex justify-content-start ">
                                            <button @click="prev_step(2)" class="btn btn-primary"><i class="fa fa-angle-left"></i> Anterior</button>
                                        </div>
                                        <div class="col-6 d-flex justify-content-end"
                                        v-if="datos.actividad_mens_selected">
                                            <button @click="next_step(3)" class="btn btn-primary">Siguiente <i class="fa fa-angle-right"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div v-show = "steps.lugar_destino">
                                    <div class="row">
                                        <div class="col-md-4 form-group">
                                            <label for="ciudad">Ciudad</label>
                                            <select v-model="datos.ciudad_receptor" name="" id="ciudad" class="form-control">
                                                <option value="monteria">Montería</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="nombre">Nombre de quien recibe</label>
                                            <input v-model="datos.nombre_receptor" class="form-control" id="nombre" placeholder="" value="" type="text">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="cedula">Cédula</label>
                                            <input v-model="datos.cedula_receptor" class="form-control" id="cedula" placeholder="" value="" type="text">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 form-group">
                                            <label for="Barrio">Barrio</label>
                                            <select class="select2 select2receptor form-control" name="" id="">
                                                <option value=""></option>
                                                <option v-for="barrio in BarriosDB" :value="barrio" v-text="barrio"></option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="Teléfono">Teléfono</label>
                                            <input v-model="datos.telefono_receptor" class="form-control" id="Teléfono" placeholder="" value="" type="text">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="Dirección">Dirección</label>
                                            <input v-model="datos.direccion_receptor" class="form-control" id="Dirección" placeholder="" value="" type="text">
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-6 d-flex justify-content-start ">
                                            <button @click="prev_step(3)" class="btn btn-primary"><i class="fa fa-angle-left"></i> Anterior</button>
                                        </div>
                                        <div class="col-6 d-flex justify-content-end"
                                        v-if="datos.ciudad_receptor && datos.nombre_receptor && datos.cedula_receptor && datos.barrio_receptor && datos.telefono_receptor && datos.direccion_receptor">
                                            <button @click="next_step(4)" class="btn btn-primary">Siguiente <i class="fa fa-angle-right"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div v-show = "steps.mensajero_return">
                                    <div class="row ">
                                        <div class="col-12 d-flex justify-content-center">
                                            <button @click="datos.mensajero_return = 'SI' " class="btn btn-default" :class="{'btn btn-warning' : datos.mensajero_return == 'SI' }">SI</button>
                                            <button @click="datos.mensajero_return = 'NO' " class="btn btn-default" :class="{'btn btn-warning' : datos.mensajero_return =='NO' }">NO</button>
                                        </div>
                                    </div>

                                    <div class="row " v-if="!processing">
                                        <div class="col-6 d-flex justify-content-start ">
                                            <button @click="prev_step(3)" class="btn btn-primary"><i class="fa fa-angle-left"></i> Anterior</button>
                                        </div>
                                        <div class="col-6 d-flex justify-content-end"
                                        v-if="datos.mensajero_return">
                                            <button @click="next_step(5)" class="btn btn-primary">Finalizar<i class="fa fa-angle-right"></i></button>
                                        </div>
                                    </div>

                                    <div class="row mt-30" v-if="processing">
                                        <div class="col">
                                            Enviando solicitud, por favor espere...
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
             
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

    {{-- MODAL MODAL --}}
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">¿Cómo solicitar el servicio de mensajería?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <div class="embed-responsive embed-responsive-16by9">
                <video class="" src="http://mefex.com.co/wp-content/uploads/2020/07/TUTORIAL.mp4" controls ></video>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
    </div>


  
</div>

<script>
    var Sair = new Vue({
        el: "#SolMensajeria",
        data:{

            BarriosDB:[ 
                "EL POBLADO","EL DORADO","NUEVA HOLANDA","LA RIBERA","REPÚBLICA DE PANAMÁ","URBANIZACIÓN PUENTE NÚMERO UNO",
                "URBANIZACIÓN BETANCÍ","URBANIZACIÓN EL NÍSPERO","URBANIZACIÓN CARACOLÍ","MI RANCHITO","NUEVO HORIZONTE","RANCHO GRANDE",
                "CASA FINCA","LA PALMA","LA VID","VILLA NAZARET","URBANIZACIÓN SAN JERÓNIMO","URBANIZACIÓN EL PORTAL II","LOS ÉBANOS",
                "LA FE","URBANIZACIÓN LA NAVARRA","SECTOR EL CAMPANO","URBANIZACIÓN LOS COLORES","URBANIZACIÓN EL PORTAL I","MANUEL JIMÉNEZ",
                "LA ESPERANZA","VILLA LUZ","EL TAMBO","URBANIZACIÓN MANUEL ANTONIO BUELVAS","JUAN XXIII","EL BONGO",
                "EL ROSARIO","MAGDALENA","LA ALBORAYA","CAMPO ALEGRE","EL CAMPANO","MINUTO DE DIOS","CASITA NUEVA",
                "URBANIZACIÓN LAS VIÑAS","LA ESMERALDA","URBANIZACIÓN VILLA REAL","VILLA NUEVA","EL AMPARO",
                "URBANIZACIÓN EL PUENTE N°2","TARUEL","CIUDADELA CONFACOR","BARRIO RIO DE JANEIRO",
                "BRISAS DEL SINÚ","SANTA FÉ","SIMÓN BOLÍVAR","LA COQUERA","SANTA LUCÍA","LA GRANJA","BUENAVISTA",
                "EL TENDA","SANTANDER","SAN MARTÍN","PASTRANA BORRERO","POLICARPA","GRANADA","MIRAFLORES",
                "URBANIZACIÓN SAMARIA","GUADALAJARA","P-5","ANDALUCÍA","BOSTON","URBANIZACIÓN LAS AMÉRICAS",
                "MOGAMBO","MOGAMBITO","EDMUNDO LÓPEZ II","GALILEA","EL PRADO","1° DE MAYO","VILLA KARIME",
                "VISTA HERMOSA","EL PARAÍSO","LAS ACACIAS","VILLA LOS ALPES","SANTA ISABEL","LOS NOGALES",
                "VEREDA TROPICAL","SANTA ROSA","PAZ DEL RIO","LA CANDELARIA","EL ENJAMBRE","LOS ROBLES I",
                "LOS ROBLES II","LA GLORIA","FURATENA","VILLA PAZ","NUEVA ESPERANZA","LOS ARAUJOS","VILLA JIMENEZ",
                "EL PRIVILEGIO","ROBINSON PITALUA","LAS COLINAS (EL CERRO)","ALFONSO LÓPEZ","2 DE SEPTIEMBRE",
                "PABLO VI","VILLA MARGARITA","LA CAMPIÑA","PANZENÚ","URBANIZACIÓN ARAUJOS","COLINA REAL","RANCHITOS",
                "NARIÑO","MONTERÍA MODERNO","OSPINA PÉREZ","EL CENTRO","LA CEIBA","EL EDÉN I","CHUCHURUBÍ",
                "LOS ÁLAMOS","COLÓN","LA CHARME","OBRERO","14 DE JULIO","LA VICTORIA","RISARALDA","BALBOA",
                "CHAMBACÚ","SANTA CLARA","PUEBLO NUEVO","EL COLISEO","LA JULIA","URBINA","COSTA DE ORO",
                "PASATIEMPO","TACASUÁN II","LOS URAPANES","URBANIZACIÓN CASASUÁN","6 DE MARZO","EDMUNDO LÓPEZ",
                "URBANIZACIÓN EL DIAMANTE","URBANIZACIÓN TACASUÁN","URBANIZACIÓN ROSALES DE TACASUÁN",
                "EL ALIVIO","LA PRADERA","VILLA ROCÍO","CANTA CLARO","URBANIZACIÓN VILLA ANA","URBANIZACIÓN EL LAGUITO",
                "URBANIZACIÓN CUNDAMA","VILLA MERY","VILLA ROSARIO","VILLA ARLETH","NUEVA COLOMBIA","LOS CORALES",
                "NUEVA JERUSALÉN","NUEVA BELÉN","VILLA CARIBE","CARIBE","FURATENA II","DAMASCO","SAN CRISTOBAL",
                "SUCRE (PLAYA BRIGIDA-INVASIÓN)","SUCRE","LUIS CARLOS GALÁN","EL CÁRMEN","INDUSTRIAL","LOS LAURELES",
                "ALTOS DEL COUNTRY","VILLA DEL RIO","ALAMEDAS DEL SINU","PRADO NORTE","URBANIZACIÓN BONANZA",
                "VILLA SOFÍA","PORTAL DE NAVARRA","EL LIMONAR","SAN ANTONIO","25 DE AGOSTO","SANTA ELENA",
                "FILADELFIA","SANTA ELENA","VILLA VENECIA","URBANIZACIÓN SANTA TERESA","VILLA NATALIA",
                "URBANIZACIÓN VILLA SORRENTO","URBANIZACIÓN VILLA FÁTIMA","SANTA BÁRBARA","VILLANOVA",
                "SAN JOSÉ","EL EDÉN","LA FLORESTA","EL MORA","ALTOS DE LA FLORESTA","URBANIZACIÓN BRIZALIA",
                "URBANIZACIÓN VERSALLES","URBANIZACIÓN EL PRIVILEGIO","URBANIZACIÓN VILLA CAMPESTRE",
                "URBANIZACIÓN VILLA SABANA","PORTAL DE LA CANDELARIA","MONTEVERDE","LOS ÁNGELES",
                "LA CASTELLANA","URBANIZACIÓN CASTILLA REAL","BARRIO LA ESPAÑOLA","LOS ALCÁZARES",
                "URBANIZACIÓN CASTILLA LA NUEVA","URBANIZACIÓN SEVILLA","SAN JERÓNIMO",
                "LOS ROBLES DEL NORTE","SEVILLA","BOSQUES DE SEVILLA","VILLA NORTE","BARRIO CANTABRIA","LOS MANGOS",
                "RANCHOS DEL INAT","CALIFORNIA","URBANIZACIÓN LOS BONGOS","EL RECREO","ROBLES DEL NORTE","PICACHO",
                "URBANIZACION DULCE HOGAR","VEREDA HORIZONTE","URBANIZACION VILLA CIELO","TERMINAL","URBANIZACIÓN SAN FRANCISCO",
                "PAZ DEL NORTE","LOS CRISTALES","20 DE JULIO","CAMILO TORRES","LAS PARCELAS","MOCARÍ","7 DE MAYO",
                "EL BOSQUE","PORTAL DEL NORTE","BOSQUES DE CASTILLA","EL CEIBAL","BARRIO PALMA 9","VILLA IDALIA",
                "EL TENDAL","ANDA LUCÍA","URBANIZACIÓN VILLA ANA I","URBANIZACIÓN VILLA ANA II","EL EDÉN II",
                "COMUNA 1","COMUNA 2","COMUNA 3","COMUNA 4","COMUNA 5","COMUNA 6","COMUNA 7","COMUNA 8","COMUNA 9","Kilometro 12"
            ],

            steps : {datos_cliente : true,
                    lugar_origen : false,
                    actividad_mensajero: false, 
                    lugar_destino : false,
                    mensajero_return : false,
                    fin: false},

            step_subtitulo : "(Paso 1 de 5) Tus datos personales",

            actividades_mensajero : [
                {name : "Diligencia", selected: false},
                {name: "Documentos", selected: false},
                {name: "Encargo", selected: false},
                {name: "Radicaciones", selected: false},
                {name: "Pagos", selected: false}
            ],

            datos : {
                // datos personales remitente:
                ciudad_remitente: null,
                nombre_remitente: null , 
                cedula_remitente:null,
                // dirección de recoleccion
                barrio_remitente: null,
                telefono_remitente: null,
                direccion_remitente:null,
                // actividad que realizará el mensajero
                actividad_mens_selected : null,
                descripcion_actividad: null,
                // DATOS RECEPTOR -- ¿quien recibe?
                ciudad_receptor: null,
                nombre_receptor: null , 
                cedula_receptor:null,
                // dirección de recoleccion
                barrio_receptor: null,
                telefono_receptor: null,
                direccion_receptor:null,
                // el mensajero debe retornar: ¡???
                mensajero_return : null
            },

            processing: false
            
        },
        methods: {
            ver_tutorial() {
                $('.modal').modal('show'); // abrir
            },
            next_step : function (step){
                switch (step) {
                    case 1:
                        Sair.steps.datos_cliente = false;
                        Sair.steps.lugar_origen = true;
                        Sair.step_subtitulo = "(Paso 2 de 5) Dirección de recolección"    
                    break;
                    case 2:
                        Sair.steps.datos_cliente = false;
                        Sair.steps.lugar_origen = false;
                        Sair.steps.actividad_mensajero = true;
                        Sair.step_subtitulo = "(Paso 3 de 5)  ¿Qué actividad realizará el mensajero?"    
                    break;
                    case 3:
                        Sair.steps.datos_cliente = false;
                        Sair.steps.lugar_origen = false;
                        Sair.steps.actividad_mensajero = false;
                        Sair.steps.mensajero_return = false;
                        Sair.steps.lugar_destino = false;

                        if(Sair.datos.actividad_mens_selected == "Pagos"){
                            Sair.steps.mensajero_return = true;
                            Sair.step_subtitulo = "¿Una vez el mensajero haya realizado la diligencia debe volver a la dirección de recolección?"
                        }else{
                            Sair.steps.lugar_destino = true;
                            Sair.step_subtitulo = "(Paso 4 de 5) ¿Quién recibe?"
                        }
                    break;

                    case 4:
                        Sair.steps.datos_cliente = false;
                        Sair.steps.lugar_origen = false;
                        Sair.steps.actividad_mensajero = false;
                        Sair.steps.mensajero_return = true;
                        Sair.steps.lugar_destino = false;
                        Sair.step_subtitulo = "(Paso 5 de 5) ¿Una vez el mensajero haya realizado la diligencia debe volver a la dirección de recolección?"
                    break;

                    case 5:
                        Sair.EnviarSolicitud();
                    break;
                }
            },
            

            prev_step : function (step){
                switch (step) {
                    case 1:
                    Sair.steps.datos_cliente = true;
                    Sair.steps.lugar_origen = false;
                    Sair.step_subtitulo = "Tus datos personales"
                    break;

                    case 2:
                    Sair.steps.datos_cliente = false;
                    Sair.steps.lugar_origen = true;
                    Sair.steps.actividad_mensajero = false;
                    Sair.step_subtitulo = "Dirección de recoleción"
                    break;

                    case 3:
                    Sair.steps.datos_cliente = false;
                    Sair.steps.lugar_origen = false;
                    Sair.steps.actividad_mensajero = true;
                    Sair.steps.lugar_destino = false;
                    Sair.steps.mensajero_return = false;
                    Sair.step_subtitulo = "¿Qué actividad realizará el mensajero?"    
                    break;

                    
                }
              
            },

            SelectActivMensajero : function (index){
                
                for (let i = 0; i < Sair.actividades_mensajero.length; i++) {
                    if(i == index){
                        Sair.actividades_mensajero[index]["selected"] = "border-warning";
                        Sair.datos.actividad_mens_selected  =  Sair.actividades_mensajero[index]["name"];
                    }else{
                        Sair.actividades_mensajero[i]["selected"] = false;
                    }
                }
            },

            selects_2 : function (){
                let that = this;
                $(".select2remitente").change(function(){
                    that.datos.barrio_remitente = $(".select2remitente").val();
                });
                $(".select2receptor").change(function(){
                    that.datos.barrio_receptor = $(".select2receptor").val();
                });
            },

            EnviarSolicitud : function (){
                Sair.processing = true;
                let formdata = new FormData();
                formdata.append("data" ,  JSON.stringify(Sair.datos) );
                axios.post("{{ route('guest.EnviarSolicitud') }}" , formdata).then(function(response){
                    Sair.processing = false;
                    if(response.data == "ok"){
                        Swal.fire({
                        icon: 'success',
                        title: 'Hemos recibido tu solicitud',
                        text: 'Pronto nos pondremos en contacto contigo para concretar tu pedido. Puedes hacerle seguimiento a tu pedido haciendo uso de tu número de cédula'
                        }).then(function(){
                            location.reload();  
                        })
                    }else{
                        Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Estamos presentando fallas internas, por favor cerifica que los campos estén bien diligenciados o recarga la página y vuelve a intentarlo.'
                        })
                    }
                })
                
            },

        },

        mounted() {
            this.selects_2();
        },
    })

    
    $(document).ready(function() {
        $('.select2').select2();
    });

</script>
@endsection  
