@extends('layouts.app')
@section("title", "OFICINA")
@section('content')
<br>
<div class="container mt-xl-50 mt-md-50 mt-sm-10" id="Solicitudes">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h6 v-text ="solicitudes_nuevas + ' Solicitudes nuevas'"></h6></div>

                <div class="card-body">
                    <div class="table-wrap" >
                        <table  id="tableSolicitudes" class="table table-responsive  ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fecha</th>
                                    <th>Nombre Remitente</th>
                                    <th>Tel. Remitente</th>
                                    <th>Dir. remitente</th>
                                    <th>Nombre Destinatario</th>
                                    <th>Tel. Destinatario</th>
                                    <th>Dir. Destinatario</th>
                                    <th>Estado</th>
                                    <th>acciones</th>
                                    
                                </tr>
                            </thead>
                            <tbody></tbody>
                            
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {{-- MODAL MODAL EDIT SOLICITUD--}}
    <div class="modal modalSolicitud fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" v-text="'Información solicitud #' + solicitud_selected[0] "></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-sm">
                            <div class="accordion accordion-type-2" id="accordion_2">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between activestate">
                                        <a role="button" data-toggle="collapse" href="#collapse_1i" aria-expanded="true">Seguimiento / estados</a>
                                    </div>
                                    <div id="collapse_1i" class="collapse show" data-parent="#accordion_2" role="tabpanel">
                                        <div class="card-body pa-15">
                                            <div class="row">
                                                <div class="col">
                                                 <ul class="list-group mb-15">
                                                    <li v-for="estado in estados_solicitud_selected" class="mb-5 mt-5 list-group-item list-group-inv list-group-inv-success " :class="{'list-group-inv-danger' : estado.estado == 'Cancelada'}" >
                                                        <p v-text = "estado.created_at +' | '+ estado.estado"></p>
                                                    </li>
                                                 </ul>
                                                </div>
                                             </div>    

                                             <div class="row">
                                                 <div class="col-8">
                                                    <select v-model="nuevo_estado" class="form-control" name="" id="">
                                                        <option value="">Seleccione un nuevo estado...</option>
                                                        <option value="Revisando">Revisando</option>
                                                        <option value="En curso">En curso</option>
                                                        <option value="Finalizado">Finalizado</option>
                                                        <option value="Cancelado">Cancelado</option>
                                                    </select>
                                                 </div>
                                                 <div class="col-4">
                                                    <button v-if="!processing" @click="NuevoEstado()" class="btn btn-default">Nuevo estado</button>
                                                    <button v-if="processing"  class="btn btn-default">Aplicando cambios...</button>
                                                </div>
                                             </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_4i" aria-expanded="true">Remitente</a>
                                    </div>
                                    <div id="collapse_4i" class="collapse show" data-parent="#accordion_2">
                                        <div class="card-body pa-15"> 
                                            <div class="row">
                                               <div class="col">
                                                <ul class="list-group mb-15">
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Nombre remitente: ' + solicitud_selected[4]"></p><i :class="{'fa fa-check': solicitud_selected[4] , 'fa fa-times': !solicitud_selected[4]}"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Cédula: ' + solicitud_selected[5]"></p><i :class="{'fa fa-check': solicitud_selected[5] , 'fa fa-times': !solicitud_selected[5]}"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Ciudad: ' + solicitud_selected[3]"></p><i :class="{'fa fa-check': solicitud_selected[3] , 'fa fa-times': !solicitud_selected[3]}"></i>
                                                    </li>
                                                    
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Dirección: ' + solicitud_selected[19]"></p><i :class="{'fa fa-check': solicitud_selected[19] , 'fa fa-times': !solicitud_selected[19]}"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Teléfono: ' + solicitud_selected[7]"></p><i :class="{'fa fa-check': solicitud_selected[7] , 'fa fa-times': !solicitud_selected[7]}"></i>
                                                    </li>
                                                </ul>
                                               </div>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_2i" aria-expanded="true">Destinatario</a>
                                    </div>
                                    <div id="collapse_2i" class="collapse show" data-parent="#accordion_2">
                                        <div class="card-body pa-15">
                                            <div class="row">
                                                <div class="col">
                                                 <ul class="list-group mb-15">
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Nombre destinatario: ' + solicitud_selected[12]"></p><i :class="{'fa fa-check': solicitud_selected[12] , 'fa fa-times': !solicitud_selected[12]}"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Cédula: ' + solicitud_selected[13]"></p><i :class="{'fa fa-check': solicitud_selected[13] , 'fa fa-times': !solicitud_selected[13]}"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Ciudad: ' + solicitud_selected[11]"></p><i :class="{'fa fa-check': solicitud_selected[11] , 'fa fa-times': !solicitud_selected[11]}"></i>
                                                    </li>
                                                    
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Dirección: ' + solicitud_selected[20]"></p><i :class="{'fa fa-check': solicitud_selected[20] , 'fa fa-times': solicitud_selected[20] !== 'Br/'}"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Teléfono: ' + solicitud_selected[15]"></p><i :class="{'fa fa-check': solicitud_selected[15] , 'fa fa-times': !solicitud_selected[15]}"></i>
                                                    </li>
                                                 </ul>
                                                </div>
                                             </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_3i" aria-expanded="true">Servicio</a>
                                    </div>
                                    <div id="collapse_3i" class="collapse show" data-parent="#accordion_2">
                                        <div class="card-body pa-15"> 
                                            <div class="row">
                                                <div class="col">
                                                 <ul class="list-group mb-15">
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Servicio: ' + solicitud_selected[9]"></p><i class="fa fa-check"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Descripción: ' + solicitud_selected[10]"></p><i class="fa fa-check"></i>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <p v-text="'Ida y vuelta: ' + solicitud_selected[17]"></p><i class="fa fa-check"></i>
                                                    </li>
                                                 </ul>
                                                </div>
                                             </div>    
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </div>

</div>
 
<audio class="audiotimbre" style="display: none">
<source src="{{ asset('dist/audio/timbre.mp3') }}" type="audio/mp3">
</audio>
{{-- 

<script  src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script  src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script  src="{{ asset('dist/js/dataTables-data.js') }}"></script> --}}


<script src="https://code.jquery.com/jquery-3.5.1.js"> </script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"> </script>
{{-- <script  src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script> --}}

<script>
    var Sair = new Vue({
        el: "#Solicitudes",
        data:{
            solicitud_selected: false,
            estados_solicitud_selected : [],
            nuevo_estado : null,
            processing : false,
            solicitudes_nuevas : 0
        },
        methods: {
            GetSolicitudes (){
                let get_ = "{{ route('web.GetSolicitudes') }}";
                
                let tabla = $('#tableSolicitudes').DataTable( {
                    "ajax" : get_,
                    "responsive": true,
                    "bPaginate": true,
                    "info":     true,
                    "order": [[ 0, "desc" ]],
                    dom: 'Bfrtip',
                    buttons: [
			              'excel', 'pdf', 'print'
                    ],
                    "drawCallback": function () {
			            $('.dt-buttons > .btn').addClass('btn-outline-light btn-sm');
		            },
                    "columns": [
                        { "data": 0 },
                        { "data": 2 },
                        { "data": 4 },
                        { "data": 7 },
                        { "data": 19 },
                        { "data": 12 },
                        { "data": 15 },
                        { "data": 20 },
                        { "data": 21},
                        { "data": 18 }
                    ],
    
                    // más rapidez: 
                    "deferRender":true,
                    "retrieve" : true,
                    "processing": false,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ estados",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "No encontraron registros",
                        "sInfo":           "Mostrando estados del _START_ al _END_ de un total de _TOTAL_ estados",
                        "sInfoEmpty":      "Mostrando estados del 0 al 0 de un total de 0 estados",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ estados)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                        "buttons": {
                            "copy": "Copiar",
                            "colvis": "Visibilidad",
                            "print" : "Imprimir"
                        }
                    }
                } );
                
                    tabla.ajax.reload();
                    $("#tableSolicitudes tbody").on("click" , "button.EditSolicitud",  function(){
                        var data  = tabla.row($(this).parents("tr")).data();
                        Sair.solicitud_selected = data;
                        Sair.getEstadosSolicitud();
                        $('.modalSolicitud').modal('show'); // abrir
                    })

            },

            getEstadosSolicitud (){
                Sair.estados_solicitud_selected = [];
                let formdata = new FormData();
                formdata.append("id_solicitud" , Sair.solicitud_selected[1]);
                axios.post("{{ route('web.GetEstadosSolicitud') }}" , formdata).then(function(response){
                    Sair.estados_solicitud_selected = response.data;
                })
            },

            NuevoEstado : function (){
                if(Sair.nuevo_estado){
                    Sair.processing = true;
                    let formdata = new FormData();
                    formdata.append("id_solicitud" , Sair.solicitud_selected[1]);
                    formdata.append("estado" , Sair.nuevo_estado);
                    axios.post("{{  route('web.AplicarNuevoEstado') }}" , formdata).then(function(response){
                        Sair.processing = false;
                        if(response.data == "ok"){
                            Sair.getEstadosSolicitud();
                            Sair.GetSolicitudes();
                            Swal.fire({
                            icon: 'success',
                            title: '¡Nuevo estado aplicado!'
                            })   
                        }else{
                            Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Error 500 por favor recarga la página e inténtalo nuevamente.'
                            })        
                        }
                    })
                }else{
                    Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Para aplicar un nuevo estado debe seleccionarlo primero.'
                    })
                }
                
            },

            Consultor : function (){
                    let formdata = new FormData();
                    formdata.append("consultar" , true);
                    axios.post("{{ route('web.ConsultarNuevasSolicitudes') }}", formdata).then(function(response){
                        if(response.data > 0){
                            Sair.solicitudes_nuevas = response.data;
                            $(".audiotimbre")[0].play();
                        }
                    })
                setInterval(function (){
                    let formdata = new FormData();
                    formdata.append("consultar" , true);
                    axios.post("{{ route('web.ConsultarNuevasSolicitudes') }}", formdata).then(function(response){
                        if(response.data > 0){
                            Sair.solicitudes_nuevas = response.data;
                            Sair.GetSolicitudes();
                            $(".audiotimbre")[0].play();
                        }
                    })
                },20000);
            }

        },
        mounted() {
            this.GetSolicitudes();
            this.Consultor();
        },
    })
</script>
@endsection
