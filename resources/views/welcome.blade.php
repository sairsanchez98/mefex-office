@extends('layouts.app')
@section('title' , 'SOLICITAR SERVICIO')

@section('content')
       <!-- Main Content -->
    <div  id="SolicitudServicios">
        <!-- Container -->
        <div class="container">
            <!-- Title -->
            <div class="hk-pg-header">
                <div>
                    <h2 class="hk-pg-title font-weight-600 mb-10">Solicitar servicio</h2>
                    <p>Recogemos, llevamos, entregamos, radicamos y pagamos lo que necesites en minutos.<i class="ion ion-md-help-circle-outline ml-5" data-toggle="tooltip" data-placement="top" title="Need help about earning stats"></i></p>
                </div>
                {{-- <div class="d-flex">
                    <div class="btn-group btn-group-sm" role="group">
                        <button type="button" class="btn btn-outline-secondary active">today</button>
                        <button type="button" class="btn btn-outline-secondary">week</button>
                        <button type="button" class="btn btn-outline-secondary">month</button>
                        <button type="button" class="btn btn-outline-secondary">quarter</button>
                        <button type="button" class="btn btn-outline-secondary">year</button>
                    </div>
                </div> --}}
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-xl-12">
                    
                    
                    <div class="card hk-dash-type-1 overflow-hide">
                        <div class="card-body ">
                            <div class="row d-flex justify-content-center mb-30">
                                <H4 class="text-center">Selecciona el servicio que necesites</H4>
                            </div>
                            <div class="row d-flex justify-content-center">
                                <div @click="redirect('m')" class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-30 cursor_pointer">
                                    <div class="card text-white bg-warning">
                                        <div class="card-body">
                                            <h5 class="card-title text-white">Mensajería Personal</h5>
                                            {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                                        </div>
                                    </div>
                                </div>
                                <div @click="redirect('mem')" class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-30 cursor_pointer">
                                    <div class="card text-white bg-warning">
                                        <div class="card-body">
                                            <h5 class="card-title text-white">Mensajería empresarial</h5>
                                            {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                                        </div>
                                    </div>
                                </div>
                                <div @click="redirect('mee')" class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-30 cursor_pointer">
                                    <div class="card text-white bg-warning">
                                        <div class="card-body">
                                            <h5 class="card-title text-white">Mensajería eCommerce</h5>
                                            {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                                        </div>
                                    </div>
                                </div>
                               
                            </div>

                        </div>
                    </div>
                 
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->
      
    </div>
    <!-- /Main Content -->

    <script>

        var Sair = new Vue({
            el: "#SolicitudServicios",
            data: {

            },
            methods: {
                redirect : function (view){
                    if(view == "mem"){
                        location.href="{{ route('guest.solicitarMensajeriaEmpresarial') }}";
                    }else if(view == "mee"){
                        location.href="{{ route('guest.solicitarMensajeriaEcommerce') }}";
                    }else{
                        location.href="{{ route('guest.solicitarMensajeria') }}";
                    }
                    
                }
            },
        })

    </script>


@endsection