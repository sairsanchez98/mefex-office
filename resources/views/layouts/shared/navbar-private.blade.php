
<div id="nav">
    <!-- Top Navbar -->
<nav class="navbar navbar-expand-xl navbar-dark bg-primary fixed-top hk-navbar ">
<a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
<a class="navbar-brand font-30" href="./">
    <img width="30" class="brand-img d-inline-block w-150p" src="{{ asset('dist/img/logo.png') }}" alt="brand" />
</a>

<ul class="navbar-nav hk-navbar-content ">


    <li class="nav-item dropdown dropdown-authentication">
        <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media">
                <div class="media-img-wrap">
                    <div class="avatar">
                        <img src="{{ asset('dist/img/iconos/user_default2.png') }}" alt="user" class="avatar-img img rounded-circle">
                    </div>
                    <!-- <span class="badge badge-success badge-indicator"></span> -->
                </div>
                <div class="media-body">
                    <span>
                        {{ Auth::user()->name }}
                        <i class="zmdi zmdi-chevron-down"></i></span>
                </div>
            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="dropdown-icon zmdi zmdi-power"></i>
                <span>Cerrar sesión</span>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </div>
    </li>
</ul>
</nav>
    <form role="search" class="navbar-search">
        <div class="position-relative">
            <a href="javascript:void(0);" class="navbar-search-icon"><span class="feather-icon"><i data-feather="search"></i></span></a>
            <input type="text" name="example-input1-group2" class="form-control" placeholder="Type here to Search">
            <a id="navbar_search_close" class="navbar-search-close" href="#"><span class="feather-icon"><i data-feather="x"></i></span></a>
        </div>
    </form>
    <!-- /Top Navbar -->

<!--Horizontal Nav-->
<nav class="hk-nav hk-nav-light">
<a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
<div class="nicescroll-bar">
    <div class="navbar-nav-wrap">
        <ul class="navbar-nav flex-row nav nav-tabs nav-tabs-primary"  role="tablist">
            

            <li class="nav-item" >
            <a class="{{ strpos(request()->url(), 'formularios') ? 'active' : '' }} nav-link" href="{{ route('home') }}" >
                    <span class="feather-icon"><i data-feather="box"></i></span>
                    <span class="nav-link-text">Solicitudes</span>
                </a>
            </li> 
           
           
          
            
            

        </ul>
    </div>
</div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<!--/Horizontal Nav-->
</div>

