<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/solicitar-servicio')->namespace('solicitarServicio')->group(function(){
    Route::get('mensajeria-empresarial', 'mainController@viewMenEmpresarial' )
    ->name('guest.solicitarMensajeriaEmpresarial');

    Route::get('mensajeria-ecommerce', 'mainController@viewMenEcomm' )
    ->name('guest.solicitarMensajeriaEcommerce');

    Route::get('mensajeria', 'mainController@viewMensajeria' )
    ->name('guest.solicitarMensajeria');

    Route::post('EnviarSolicitud', 'mainController@RegistrarSolicitudMensajeria' )
    ->name('guest.EnviarSolicitud');
});

Route::prefix('/seguimiento-de-pedido')->namespace('tracking')->group(function(){
    Route::get('/', 'mainController@index' )
    ->name('guest.tracking');

    Route::post('/seguir-pedido', 'mainController@seguirPedido' )
    ->name('guest.trackingIT');
});






Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/soliitudes-de-servicio')->namespace('solicitarServicio')->group(function(){
    Route::get('/lista-solicitudes', 'mainController@GetSolicitudes' )
    ->name('web.GetSolicitudes');

    Route::post('/soolicitud-estados', 'mainController@GetEstadosSolicitud' )
    ->name('web.GetEstadosSolicitud');

    Route::post('/aplciar-estado-solicitud', 'mainController@AplicarNuevoEstado' )
    ->name('web.AplicarNuevoEstado');

    Route::post('/consultar-nuevas-solicitudes', 'mainController@ConsultarNuevasSolicitudes' )
    ->name('web.ConsultarNuevasSolicitudes');

    

    
});


