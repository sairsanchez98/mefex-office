<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSolicitudesMensajeria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_solicitudes_mensajeria', function (Blueprint $table) {
            $table->id();
            $table->string("n_guia")->nullable();
            $table->string("ciudad_remitente");
            $table->string("nombre_remitente");
            $table->integer("cedula_remitente");
            $table->string("barrio_remitente");
            $table->string("telefono_remitente");
            $table->string("direccion_remitente");
            
            $table->string("actividad_mensajero");
            $table->string("descrip_activ_mensajero")->nullable();

            $table->string("ciudad_receptor")->nullable();
            $table->string("nombre_receptor")->nullable();
            $table->integer("cedula_receptor")->nullable();
            $table->string("barrio_receptor")->nullable();
            $table->string("telefono_receptor")->nullable();
            $table->string("direccion_receptor")->nullable();

            $table->string("return_mensajero");

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_solicitudes_mensajeria');
    }
}
